# Translation metric

calculate the translation quality estimation scores (as described in the [Thesis chapter 7.8](http://multitranslation.space/metric)) over the [Vector Space Model](https://en.wikipedia.org/wiki/Vector_space_model) and the correlation with BLEU.

## Example output

```
bleu ranks and below the corresponding number of total suggestions:
[2,  3,  0,  1,  3,  5,  4,  3, 1, 2,  2,  0]
[18, 17, 17, 11, 16, 16, 15, 9, 4, 17, 10, 8]

pearsonr_values:
(0.8205981815128616, 9.647653104694295e-40)

LinregressResult(slope=0.7458971966654098, 
intercept=0.17721339132657488, 
rvalue=0.8205982016997805, 
pvalue=9.647576540180548e-40, 
stderr=0.04159168982027579)
```

![alt correlation](https://gitlab.com/Bachstelze/translation-metric/raw/master/gitlab_linear_correlation.png)

## Requirements

- gensim
- nltk
- numpy
- pandas
- seaborn
- matplotlib

## TODO

- Calculate the correlation with a bigger translation set and in different languages.
- Break off the universal approach and find the best features for every language.

## Lisence

If you use this code in your work or research,
please cite this website http://multitranslation.space/metric and my thesis:

> Gruppenbasierte Umgebung für mehrsprachige Übersetzungsquellen;
> Kalle Hilsenbek; Hochschule RheinMain; 2017