#!/usr/bin/python3
#
# first Axiom: Aaron Swartz is everything
# second Axiom: The Schwartz Space is his discription of physical location
# first conclusion: His linear symmetry is the Fourier transform
# second conclusion: His location is the Montel space
# Third conclusion: His location is the Fréchet space

import csv
import ast

from gensim import corpora
from gensim.test.utils import get_tmpfile
from gensim.similarities import Similarity
from gensim.similarities import MatrixSimilarity

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk import ngrams

from scipy.stats.stats import pearsonr
from scipy.stats import linregress

import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt

import seaborn as sns

test_name = "gitlab"
test_language = 'german'

def split_words(sentence, filter_stopwords=False):
    mapping = [("-"," "), ("'"," "), ("  "," "),
        (",",""), (".",""), (".",""), ("\n","")]
    for seeking, replacement in mapping:
        sentence = sentence.replace(seeking, replacement)
    words = sentence.split(" ")

    if filter_stopwords:
        ignore_words = stopwords.words(test_language)
        filtered = [word for word in words if word not in ignore_words]
        return filtered
    return words

def get_words(sentence):
    # get single words
    # + joined bi- and trigrams
    no_stopwords = split_words(sentence, True)
    word_list = split_words(sentence, False)
    pseudo_words = word_list[:]

    ngram = ngrams(word_list, 2)
    for gram in ngram:
        pseudo_words.append('_'.join(gram))

    ngram = ngrams(word_list, 3)
    for gram in ngram:
        pseudo_words.append('_'.join(gram))

    ngram = ngrams(no_stopwords, 4)
    for gram in ngram:
        pseudo_words.append('_'.join(gram))

    return pseudo_words

def set_cosine_scores(translations):
    """
    Set the sentence quality estimation for all suggestions of one language.
    All suggestions are building together a pseudo-document.
    The single sentences are compared against this document with the cosine distance.

    You can use nltk.tokenize.word_tokenize instead of the get_words() method.

    :param translations: all suggestions already filtered for one certain language
    """

    # init the list of words for the dictionary creation
    dictionary_words = []
    # init the lists of words in the sentence structure for the creation of the corpus
    full_document = []

    # iterate over the suggestions and fill the empty lists
    for translation in translations:
        sentence = translation['phrase']
        words = get_words(sentence)
        #words = get_ngram(sentence)
        full_document.extend(words)
        dictionary_words.append(words)

    # create the dictionary, the corpus with the bag of words
    dictionary = corpora.Dictionary(dictionary_words)
    full_corpus = [dictionary.doc2bow(full_document)]
    # prepare the cosine similarity, this should fit in the RAM
    matrix_similarity = MatrixSimilarity(full_corpus, num_features=len(dictionary))

    for translation in translations:
        query = dictionary.doc2bow(get_words(translation['phrase']))
        similarity_score = matrix_similarity[query]
        translation['cosine_similarity'] = similarity_score[0]

print(test_name)
test_file_name = 'bow_'+test_name+".csv"
csv_file  = open(test_file_name, "w")
writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
writer.writerow(["original_en", "original_es", "original_fr", "original_ru", "original_ar", "translations", "correct_translation_de", "words_frequency", 'bow_similarity'])

reader = csv.reader(open("bleu.csv"), delimiter=',')
header = next(reader)

bleu_rank = []
suggestion_number = []
bleu_scores = []
bow_scores = []

for row_number, row in enumerate(reader):
    translations = ast.literal_eval(row[5])

    set_cosine_scores(translations)

    sorted_by_value = sorted(translations, key=lambda trans_dict: trans_dict['cosine_similarity'])
    for i, sorted_translation in enumerate(sorted_by_value[::-1]):
        bleu_scores.append(sorted_translation['score'])
        bow_scores.append(sorted_translation['cosine_similarity'])

        sorted_translation['bow_rank'] = i
        if i == 0:
            bleu_rank.append(sorted_translation['bleu_rank'])
            suggestion_number.append(len(translations))



    row.append(translations)
    writer.writerow(row)

csv_file.close()

print("bleu ranks and below the corresponding number of total suggestions:")
print(bleu_rank)
print(suggestion_number)

pearsonr_values = pearsonr(bleu_scores, bow_scores)
print("pearsonr_values:")
print(pearsonr_values)

lin = linregress(bleu_scores, bow_scores)
print(lin)

sim_data = pd.DataFrame({'Bleu value':bleu_scores,
'cosine document similarity':bow_scores})

figure = sns.jointplot("Bleu value", "cosine document similarity", data=sim_data, kind="reg", xlim = (0.3,1.01), ylim = (0.3,1.01))
picture_name = test_name + "_linear_correlation.png"
figure.savefig(picture_name)
